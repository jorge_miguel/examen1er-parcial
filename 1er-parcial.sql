-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-04-2019 a las 19:17:04
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `1er-parcial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `h2`
--

CREATE TABLE `h2` (
  `id` int(11) NOT NULL,
  `link` varchar(9000) NOT NULL,
  `cad_h1` varchar(9000) NOT NULL,
  `cad_h2` varchar(9000) NOT NULL,
  `cad_h3` varchar(9000) NOT NULL,
  `cad_p` varchar(9000) NOT NULL,
  `cad_a` varchar(9000) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `h2`
--

INSERT INTO `h2` (`id`, `link`, `cad_h1`, `cad_h2`, `cad_h3`, `cad_p`, `cad_a`, `fecha`) VALUES
(22, 'https://devcode.la/tutoriales/hacer-web-scraping-con-php/', 'Hacer web scraping con PHP', 'Web scraping ; Web scraping con PHP ; Web scraping con cURL y PHP', 'Cursos relacionados', 'En este tutorial aprenderemos a hacer web scraping solo con PHP y a hacer web scraping con cURL y PHP. TambiÃ©n conoceremos para quÃ© fines lo podemos utilizar. ; Para los que no estÃ©n familiarizados con el tÃ©rmino web scraping, deben saber que es un tÃ©cnica empleada para extraer informaciÃ³n de sitios web. Con unas cuantas lÃ­neas de cÃ³digo puedes recorrer el cÃ³digo fuente de una pÃ¡gina web tal y como se ve en el navegador, guardarlo en una base de datos, mostrarlo de manera idÃ©ntica en una URL propia, extraer solo informaciÃ³n importante dentro del cÃ³digo, entre otros. ; Para seguir los pasos indicados en este tutorial primero debemos tener instalado XAMPP en nuestro sistema operativo Windows, puedes utilizar otros entornos, inclusive otros sistemas operativos, solo debes asegurarte cuente con PHP. ; Para los que aÃºn no tengan instalado XAMPP en Windows, pueden revisar el siguiente tutorial: https://devcode.la/tutoriales/instalar-xampp-en-windows-7/. ; Luego, en el Panel de Control de XAMPP activaremos el mÃ³dulo Apache. ;  ; A continuaciÃ³n en nuestra carpeta htdocs, ubicada dentro de la carpeta xampp en nuestro disco C, creamos nuestra carpeta webscraping dentro de la cual crearemos el archivo index.php, luego escribiremos el siguiente cÃ³digo: ; Una vez creado el archivo index.php con su respectivo cÃ³digo, abrimos en nuestro navegador la siguiente URL http://localhost/webscraping/, que es la URL donde se muestra el resultado de nuestro cÃ³digo anterior. ;  ; Y vemos que copia de manera idÃ©ntica la pÃ¡gina web https://devcode.la, revisemos el cÃ³digo PHP para comprender lo que sucediÃ³. ; En la variable $html se almacenÃ³ el resultado de la funciÃ³n file_gets_content, lo que hace file_gets_content es convertir la informaciÃ³n de un fichero en una cadena, siendo el fichero en este caso la pÃ¡gina web https://devcode.la. ; Luego esta cadena la mostramos en nuestro documento haciendo uso de â€œechoâ€. ; Ahora veamos cÃ³mo podemos hacer web scraping haciendo uso de la librerÃ­a cURL, para esto creamos nuestro archivo curl.php dentro de la carpeta /webscraping creada anteriormente y escribimos el siguiente cÃ³digo: ; Dentro del cÃ³digo en los comentarios encontrarÃ¡s la explicaciÃ³n de cada lÃ­nea, resumiendo, primero creamos una funciÃ³n llamada curl, tambiÃ©n se pudo realizar directamente sin crear la funciÃ³n, luego iniciamos sesiÃ³n con curl_init, luego hicimos un par de configuraciones, CURLOPT_RETURNTRANSFER va relacionado a poder utilizar el resultado como cadena y CURLOPT_SSL_VERIFYPEER nos sirviÃ³ para que cURL pueda funcionar en nuestra URL a pesar de tener el protocolo HTTPS. ; Luego al final utilizamos la funciÃ³n curl creada para mostrar nuestra pÃ¡gina web escrapeada. ;  ; En conclusiÃ³n, podemos ver que existe mÃ¡s de una manera de hacer web scraping con PHP.Â Este tutorial intenta hacer un acercamiento hacia este tema, sin embargo existen mÃ¡s posibilidades de cÃ³mo tratar la informaciÃ³n escrapeada. ; Recuerda ademÃ¡s que en nuestra plataforma puedes encontrar un curso de Fundamentos de PHP por sÃ­ no estas familiarizado con este lenguaje de programaciÃ³n y deseas aprenderlo desde cero. ; AyÃºdanos a llegar a mÃ¡s personas ; CMO y Co-fundador de Devcode.la, Google Adwords Certified, Google Analytics Certified, Hootsuite Certified, con experiencia y apasionado por el Marketing digital, HTML5, CSS3, JavaScript y PHP.', 'https://devcode.la/tutoriales/instalar-xampp-en-windows-7/ ; http://localhost/webscraping/ ; https://devcode.la ; Fundamentos de PHP ; Tweet ; @jjosepino ; Follow @devcodela ; CachÃ© de lado del servidor con Express en NodeJS ; Vincular eventos a elementos con jQuery ; Operador condicional ternario en JavaScript ; Fundamentos de PHP Premium ; Curso de POO en PHP Premium', '2019-04-28 16:52:59');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `h2`
--
ALTER TABLE `h2`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `h2`
--
ALTER TABLE `h2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
